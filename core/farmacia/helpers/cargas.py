from farmacia.models import Droga, Drogueria


def cargar_droga_base(nombre,cantidad,drogueria_id):
    lista_nombres = Droga.objects.all()
    
    for item in lista_nombres:
        if item.nombre == nombre and item.cantidad == int(cantidad) and item.n_drogueria_id == int(drogueria_id):
            raise Exception("ERROR Nombre,cantidad y drogueria ya existen")

    droga = Droga() #instancias el objeto
    
    droga.nombre = nombre #variable droga.nombre le damos el "nombre" de la funcion
    droga.cantidad = cantidad
    droga.n_drogueria_id = drogueria_id
    droga.save()
    
def cargar_drogueria_base(nombre):
    lista_nombres_drogueria = Drogueria.objects.all()

    for item in lista_nombres_drogueria:
        if item.nombre == nombre:
            raise Exception("ERROR EL NOMBRE DE LA DROGUERIA YA EXISTE")

    drogueria = Drogueria()  # instancias el objeto
    drogueria.nombre = nombre  # variable droga.nombre le damos el "nombre" de la funcion
    drogueria.save() #guarda en la BD


def cargar_drogamodi_base(droga ,nombre , cantidad, drogueria_id):
    lista_nombres = Droga.objects.all()

    for item in lista_nombres:
        if item.nombre == nombre and item.cantidad == int(cantidad) and item.n_drogueria_id == int(drogueria_id):
            raise Exception("ERROR Nombre,cantidad y drogueria ya existen")

    droga.nombre = nombre  # variable droga.nombre le damos el "nombre" de la funcion
    droga.cantidad = cantidad
    droga.n_drogueria_id = drogueria_id
    droga.save()

    


    

