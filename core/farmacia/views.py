from django.shortcuts import render, redirect
from farmacia.models import Droga, Drogueria
from farmacia.helpers.cargas import cargar_droga_base, cargar_drogueria_base, cargar_drogamodi_base
from django.db import IntegrityError

# Create your views here.
def cargar_droga(request):

    lista_droga = Droga.objects.all()
    lista_drogueria = Drogueria.objects.all()

    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre') #VARIABLE DE HTML
            cantidad = request.POST.get('cantidad')
            drogueria_id = request.POST.get('drogueria_id')

            cargar_droga_base(nombre,cantidad,drogueria_id) #FUNCION CARGAR DROGA
    
        except IntegrityError as otroErr:
            err = 'HAY UN ERROR BD EN LA PAGINA'
            return render(request,'cargar_droga.html',{'lista_droga':lista_droga,'lista_drogueria':lista_drogueria , 'error':str(otroErr)}) #llaves

        except Exception as err:
            return render(request,'cargar_droga.html',{'lista_droga':lista_droga,'lista_drogueria':lista_drogueria , 'error':str(err)})
        

    return render(request, 'cargar_droga.html',{'lista_droga':lista_droga,'lista_drogueria':lista_drogueria})


def cargar_drogueria(request):
    lista_drogueria = Drogueria.objects.all()

    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre')  # VARIABLE DE HTML
            cargar_drogueria_base(nombre)  # FUNCION CARGAR DROGUERIA

        except IntegrityError as otroErr:
            #err = 'HAY UN ERROR EN LA BASE DE DATOS DROGUERIA'
            return render(request, 'cargar_drogueria.html', {'lista_drogueria': lista_drogueria, 'error': str(otroErr)})  # llaves

        except Exception as err:
            return render(request, 'cargar_drogueria.html', {'lista_drogueria': lista_drogueria, 'error': str(err)})

    return render(request, 'cargar_drogueria.html', {'lista_drogueria': lista_drogueria})

def modificar_droga(request, id_droga):
    droga = Droga.objects.get(id=id_droga)
    lista_drogueria = Drogueria.objects.all()

    if request.method == 'POST':
        nombre = request.POST.get('nombre')
        cantidad = request.POST.get('cantidad')
        drogueria_id = request.POST.get('drogueria_id')
        cargar_drogamodi_base(droga, nombre,cantidad,drogueria_id)

        return redirect('/droga/cargar')

    return render(request, 'cargar_droga_modificar.html',{'droga':droga,'lista_drogueria':lista_drogueria})



